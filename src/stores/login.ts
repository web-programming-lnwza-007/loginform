import { ref, computed } from "vue";
import { defineStore } from "pinia";

export const useLoginStore = defineStore("login", () => {
  const loginname = ref("");
  const isLogin = computed(() => {
    // loginname is not empty
    return loginname.value !== "";
  });
  const login = (userName: string): void => {
    loginname.value = userName;
    localStorage.setItem("loginname", userName);
  };
  const logout = (): void => {
    loginname.value = "";
    localStorage.removeItem("loginname");
  };
  const loadData = () => {
    loginname.value = localStorage.getItem("loginname") || "";
  };

  return { loginname, isLogin, logout, loadData, login };
});
